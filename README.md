# rkDomain

Script Bash para crear un archivo de configuración de proxy nginx + nodejs.

--

Primer script tocando Bash, habrá muchos fallos lo más probable y no será mejor forma de hacerlo.

--

+ Crea/elimina un archivo de configuración proxy en NGINX y una app NodeJS.
+ Crea/elimina el usuario sFTP
+ Crea/elimina la carpeta /var/www/%usuario% del usuario sFTP.
+ Crea una plantilla básica de app NodeJS + ExpressJS asignando el puerto.
+ Crea/elimina un usuario y base de datos.
+ Habilitar/deshabilitar SSL.

# Comandos

## Crear dominio

### Crear dominio
sudo ./rkDomain domain add ejemplo.rayko.me 3000

## Eliminar dominio
sudo ./rkDomain domain delete ejemplo.rayko.me

## Listar dominios
sudo ./rkDomain list

## Ejecutar APP
sudo ./rkDomain run ejemplo.rayko.me

## Crear DB
sudo ./rkDomain db add ejemplo.rayko.me

## Borrar DB
sudo ./rkDomain db delete ejemplo.rayko.me

## Habilitar SSL
sudo ./rkDomain ssl add ejemplo.rayko.me

## Deshabilitar SSL
sudo ./rkDomain ssl delete ejemplo.rayko.me

# Bugs
- [ ] Se crean \n al habilitar/deshabilitar SSL.

# To-Do
- [ ] Registrar el SSL automáticamente con el CertBot.
- [ ] Rehacer el comando run.
- [ ] Agregar parámetros a los comandos.
- [ ] Agregar redirección http->https.
- [ ] Insertar los parámetros con cualquier orden.
- [x] Crear categorias por opción (rkDomain db -a %dominio%, rkDomain ssl -d %dominio%...)