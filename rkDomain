#!/bin/bash

echo "###############################################################"
echo "############       rkDomain - Manager v2" 
echo "###############################################################"

# Se comprueba que el usuario es root.
if [[ $EUID != 0 ]]; then

	echo -e "\n> Es necesario ejecutar el script como root.\n\n" 
	exit 1

fi

# Indica la ruta donde se guardarán los archivos de configuración de NGINX.
dirSiteAvailable="/etc/nginx/sites-available/domains"

# Nombre del archivo de configuración de NGINX
fileSiteAvailable="/etc/nginx/sites-available/domains/$3"

# Nombre del enlace simbólico vinculado al archivo de configuración de NGINX.
fileSiteEnabled="/etc/nginx/sites-enabled/$3"

# Ruta donde se guardarán el código de las webs.
dirWWW="/var/www/$3"

# Comprueba si el dominio indicado existe o no (con -c cuentas las ocurrencias, 0 ó 1).
checkDomain=$(grep -c "^$3:" /etc/passwd)

section=$1
sectionType=$2

wwwOrNot=""

if [[ $1 == '111' ]]; then

	wwwOrNot="	if ( \$host ~ ^www\.(?<domain>.+)$ ) {
		return 301 \$scheme://\$domain\$request_uri;
	}"
fi


# Función para verificar si existe un dominio, y dependiendo del tipo de ejecución (creación o borrado) se parará si encuentra o no un dominio.
CheckDomain(){
	
	# Se comprueba que pone el parámetro de dominio.
	if [[ -z "$2" ]]; then
		echo -e "\n> [ERROR] Tienes que indicar un dominio.\n"
		exit 1
	fi

	# Se comprueba si existe el dominio cuando se está creando, en caso afirmativo parará la ejecución del script.
		# Si se va a borrar hará el funcionamiento al revés, si no existe parará la ejecución.
	if [[ "$sectionType" == "add" || "$sectionType" == "a" ]]; then

		if [[ "$checkDomain" == "1" ]]; then
		
			echo -e "\n> [ERROR] Ya existe el dominio $2.\n\n"
			exit 1
		
		fi
	
	else
		
		if [[ "$checkDomain" == "0" ]]; then
		
			echo -e "\n> [ERROR] No existe el dominio $2.\n\n"
			exit 1
		
		fi
	
	fi

}

CheckPort(){

	# Comprueba que el parámetro $2 tiene un puerto.
	if [[ -z "$2" ]]; then
		echo -e "\n> [ERROR] Tienes que indicar un puerto.\n\n"
		exit 1
	fi

	# Bucle que recorre la lista de dominios.
	for i in $( ls $dirSiteAvailable); do
		# Busca el puerto pasado por el parámetro, en caso de existir, parará la ejecución.
		check=$(cat $dirSiteAvailable/$i | grep "localhost:$1;")

		if [[ ! -z "$check" ]]; then
			echo -e "\n> [ERROR] Ya existe el puerto.\n\n"
			exit 1
		fi
	done

}

# Función que guarda la lógica de creación de dominio.
RunCreate() {
	
	echo -e "\n> [OK] Creando cuenta FTP."

	# Crea un nuevo dominio (usuario) sin shell ni home, en el grupo ftpaccess y sin contraseña.
	adduser $2 --force-badname --ingroup ftpaccess --shell /sbin/nologin --no-create-home --gecos "" --disabled-password >> /dev/null
	echo -e "\n> [OK] Cuenta FTP $2 creada."

	# Pide una contraseña.
	echo
	read -s -p "> Contraseña del usuario FTP: "  password
	echo
	read -s -p "> Repite la contraseña FTP: "  rpassword
	echo

	# Se comprueba que la contraseña sean iguales, si no lo son parará la ejecución.
	if [[ $password != $rpassword ]]; then
		echo -e "\n> [ERROR] Las contraseñas no coinciden."
		exit 1
	fi

	# Se cambia la contraseña del dominio.
	echo $2:$password | chpasswd >> /dev/null
	echo -e "\n> [OK] Contraseña del usuario $2 configurada."

	# Crea la carpeta donde se alojará la web.
	mkdir -p $dirWWW/html
	echo -e "\n> [OK] Directorio WWW $dirWWW/html creado."

	if [[ "$3" == "--createAppJS" ]] ; then
		
		echo -e "\n> [OK] Creando plantilla AppJS."

		CreateAppJS $2

	else
		
		echo -e "\n> [SKIP] Creando plantilla AppJS."
	
	fi

	# Da los permisos al grupo FTP del dominio (usuario).
	chown -R $2:ftpaccess $dirWWW/html
	echo -e "\n> [OK] Permisos WWW a $dirWWW/html aplicados."

	# Se crea el archivo del dominio
	touch $fileSiteAvailable
	echo -e "\n> [OK] Archivo $fileSiteAvailable creado."

	# Contenido del archivo de configuración de NGINX.
	template="# ${2} - ${3}
server {
    listen 80;
    server_name ${2};

    access_log /var/log/nginx/${2}.access.log;
    error_log /var/log/nginx/${2}.error.log;

    location / {
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarder-For \$proxy_add_x_forwarded_for;
        proxy_set_header Host \$http_host;
        proxy_set_header X-NginX-Proxy true;

        proxy_pass http://localhost:${3};
        proxy_redirect off;
    }
}"

	# Crea el archivo proxy NGINX.
	echo "$template" >> $fileSiteAvailable
	echo -e "\n> [OK] Archivo $fileSiteAvailable configurado."

	# Crea en enlace simbólico del archivo NGINX para habilitar el dominio desde NGINX.
	ln -s $fileSiteAvailable $fileSiteEnabled
	echo -e "\n> [OK] Archivo $fileSiteEnabled creado."

	# Reinicia el servicio NGINX.
	systemctl restart nginx
	echo -e "\n> [OK] NGINX reiniciado."

	if [[ "$3" == "--createDB" ]] ; then
		
		echo -e "\n> [OK] Creando usuario y tabla de DB."

		CreateDB $2

	else
		
		echo -e "\n> [SKIP] Creando usuario y tabla de DB."
	
	fi

}

# Función que guarda la lógica de borrado de dominio.
RunDelete() {

	# Borra el usuario y busca si no ha habido errores.
	delUser=$(deluser $1 2> /dev/null | grep -c "Done.")

    # Comprueba si se ha borrado bien.
	if [[ $delUser == "1" ]]; then
	
		echo -e "\n> [OK] Cuenta FTP borrada."
	
	else
	
		echo -e "\n> [ERROR] Cuenta FTP no borrada. Proceso parado. Revisa si está en uso la cuenta $1.\n\n"
		exit 1
	
	fi

	# Comprueba si existe su carpeta WWW, si no existe no lo borra, lógicamente.
	if [ ! -d $dirWWW ]; then
	
		echo -e "\n> [SKIP] No existe la carpeta de WWW de $1"
	
	else 
	
		# Borra recursivamente su carpeta WWW.
		rm -R $dirWWW
		echo -e "\n> [OK] Carpeta $dirWWW borrada."
	
	fi

	# Comprueba si el archivo de configuración NGINX del dominio existe.
	if [ ! -e $fileSiteAvailable ]; then
	
		echo -e "\n> [SKIP] No existe el archivo $fileSiteAvailable."
	
	else
	
		# Borra el archivo del sitio nginx
		rm $fileSiteAvailable
		echo -e "\n> [OK] Archivo $fileSiteAvailable borrado."
	
	fi

	# Comrprueba si el enlace simbólico del archivo de configuración NGINX del dominio existe.
	if [ ! -L $fileSiteEnabled ]; then
	
		echo -e "\n> [SKIP] No existe el archivo $fileSiteEnabled."
	
	else
	
		# Borra el enlace simbólico del archivo de sitio nginx.
		rm $fileSiteEnabled
		echo -e "\n> [OK] Archivo $fileSiteEnabled borrado."
	
	fi

	echo -e "\n> [OK] Dominio $1 borrado."

	# Reinicia el servicio NGINX.
	systemctl restart nginx
	echo -e "\n> [OK] NGINX reiniciado."

	if [[ "$2" == "--deleteDB" ]] ; then
	
		echo -e "\n> [OK] Borrando usuario y tabla de DB."

		DeleteDB $2
	
	else
	
		echo -e "\n> [SKIP] Borrando usuario y tabla de DB."
	
	fi

}

# Función que guarda la lógica para listar los dominios creados a través de este script.
RunGetList(){
	
	echo -e "\n>> Dominios creados:\n"

	# Bucle que recorre la carpeta de archivos de configuración NGINX de los dominios.
	for i in $( ls $dirSiteAvailable ); do

		# Guarda el puerto de cada dominios y lo imprime.
		port=$(cat $dirSiteAvailable/$i | grep "proxy_pass http://localhost:")

		echo -e "\tNombre [$i] puerto [${port:36:-1}]"
	done

	echo ""

}

RunDomain(){

	#TODO Rehacer el Run. 
	echo -e "\n> [WIP] Comando en construcción.\n"
	exit 1

}

CreateDB(){

	# Pide una contraseña.
	echo
	read -s -p "> Contraseña del usuario DB: "  password
	echo
	read -s -p "> Repite la contraseña DB: "  rpassword
	echo -e ""

	# Se comprueba que la contraseña sean iguales, si no lo son parará la ejecución.
	if [[ $password != $rpassword ]]; then
	
		echo -e "\n> [ERROR] Las contraseñas no coinciden."
		exit 1
	
	fi

	commands="CREATE DATABASE \`${1}\`;
			  CREATE USER '${1}' IDENTIFIED BY '${password}';
			  GRANT USAGE ON *.* TO '${1}'@'%' IDENTIFIED BY '${password}';
			  GRANT ALL privileges ON \`${1}\`.* TO '${1}'@'%';
			  FLUSH PRIVILEGES;"

	echo "${commands}" | sudo mysql -u root

	echo -e "\n> [OK] Creado usuario y DB para $1.\n"

}

CreateAppJS(){

cd $dirWWW/html

echo "--install.modules-folder \"$dirWWW/html/node_modules\"" >> $dirWWW/html/.yarnrc

yarn add express --save >> /dev/null
echo -e "\n> [OK] ExpressJS instalado en la carpeta $dirWWW."

echo "const express = require('express');
		const app = express();
		const port = $3;

		app.get('/', (req, res) => res.send('Hello World!'));

		app.listen(port, () => console.log(\`Example app listening on port \${port}!\`));" >> $dirWWW/html/app.js

echo -e "\n> [OK] Archivo app.js creado."


}

DeleteDB(){

	commands="DROP USER \`${1}\`@'%' ;
			  DROP DATABASE \`${1}\`;"

	echo "${commands}" | sudo mysql -u root

	echo -e "\n> [OK] Borrado usuario y DB de $1.\n"

}

AddSSL(){

	file=$(cat $fileSiteAvailable)

	echo -n "" > $fileSiteAvailable

	_content=$(echo "$file" | sed  '/server_name/a \\n    ssl_certificate /etc/letsencrypt/live/'"$1"'/fullchain.pem; # SSL\
    ssl_certificate_key /etc/letsencrypt/live/'"$1"'/privkey.pem; # SSL\
    include /etc/letsencrypt/options-ssl-nginx.conf; # SSL\
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # SSL')
		
	echo "${_content/80;/443 ssl;}" >> $fileSiteAvailable

}

DeleteSSL(){

	file=$(cat $fileSiteAvailable)

	echo -n "" > $fileSiteAvailable

	_content=$(echo "$file" | sed  '/# SSL/d')

	echo "${_content/443 ssl;/80;}" >> $fileSiteAvailable

}

############
### Menús
############

RunMenuDomain(){

	echo -e "> $section - $sectionType"

	case $1 in
		add|a)
			CheckDomain $1 $2

			CheckPort $1 $3

			RunCreate $1 $2 $3
			;;
		delete|d)
			# Ejecuta y pasa el tipo de ejecución del script y el dominio.
			CheckDomain $1 $2

			# Ejecuta y pasa el dominio.
			RunDelete $2 $3
			;;
		*)
			echo -e "\n> Opción no válida. Más información poniendo rkDomain domain help.\n\n"
			exit 1
			;;
	esac

}

RunMenuDB(){
	
	if [[ -z "$2" ]]; then
	
		echo -e "\n> [ERROR] Tienes que indicar un dominio.\n"
		exit 1
	
	fi

	case $1 in
		add|a)
			CreateDB $2
			;;
		delete|d)
			# Ejecuta y pasa el dominio.
			DeleteDB $2
			;;
		*)
			echo -e "\n> Opción no válida. Más información poniendo rkDomain help.\n\n"
			exit 1
			;;
	esac

}

RunMenuSSL(){

	echo -e "> $section - $sectionType"

	case $1 in
		add|a)
			AddSSL $2
			echo -e "\n> [OK] SSL habilitado en $1."

			# Reinicia el servicio NGINX.
			systemctl restart nginx
			echo -e "\n> [OK] NGINX reiniciado."
			;;
		delete|d)
			DeleteSSL $2
			echo -e "\n> [OK] SSL deshabilidado en $1."

			# Reinicia el servicio NGINX.
			systemctl restart nginx
			echo -e "\n> [OK] NGINX reiniciado."
			;;
		*)
			echo -e "\n> Opción no válida. Más información poniendo rkDomain ssl help.\n\n"
			exit 1
			;;
	esac

}

RunMenuHelp(){

	echo -e "\ndomain\t\t - crea o elimina un dominio"
	echo -e "\tadd\t - crea un dominio."
	echo -e "\tdelete\t - elimina un dominio.\n"

	echo -e "ssl\t\t - habilita o deshabilita el SSL del dominio"
	echo -e "\tadd\t - habilita el SSL."
	echo -e "\tdelete\t - deshabilida el SSL.\n"

	echo -e "db\t\t - crea o elimina un usuario y tabla del dominio."
	echo -e "\tadd\t - crea el usuario y tabla."
	echo -e "\tdelete\t - elimina el usuario y tabla.\n"

	echo -e "run\t\t - ejecuta la aplicación del dominio (NodeJS)"
	echo -e "list\t\t - lista los dominios y el puerto de la aplicación NodeJS.\n"

	exit 1
}

#			  1    2    3      4
# rkDomain domain add rayko.me 3000

case $1 in

     domain)
		RunMenuDomain $2 $3 $4
		;;
     ssl)
		RunMenuSSL $2 $3
		;;
     db)
		RunMenuDB $2 $3
		;; 
     run)
		RunDomain $3
		;; 
     list)
		RunGetList
		;; 
     help)
		RunMenuHelp
		;; 
     *)
		echo -e "\n> Opción no válida. Más información poniendo rkDomain help\n\n"
		exit 1
		;;

esac